# Ansible Role Linux Percona MySQL

## General Information

**Author:**     Jesús Alejandro Sánchez Dávila

**Maintainer:** Jesús Alejandro Sánchez Dávila

**Email:**      [jsanchez.consultant@gmail.com](mailto:jsanchez.consultant@gmail.com)

## Description

This role installs the Percona PostgreSQL on Linux machines.

This role was created with `molecule` with the Docker driver to allow for pre-testing. It can be added/installed via ansible-galaxy to make use of it as a building block from complex playbooks.

## Supported Platforms

This role has been tested on:

- _Alma Linux 8_
- _Amazon Linux 2_
- _Amazon Linux 2023_
- _CEntOS 7_
- _Opensuse Leap 15_
- _Rocky Linux 8_
- _Ubuntu 20.04_
- _Ubuntu 22.04_

## Usage

### Role Requirements File

This role can be added to a `requirements.yml` file as follows:

    # Silvarion's Linux Percona Repos
    - src: git@gitlab.com:silvarion/ansible/roles/lnx_percona_repos.git
      scm: git
      version: "main"  # quoted, so YAML doesn't parse this as a floating-point value
    
    # Silvarion's Linux Percona PostgreSQL
    - src: git@gitlab.com:silvarion/ansible/roles/lnx_percona_mysql.git
      scm: git
      version: "main"  # quoted, so YAML doesn't parse this as a floating-point value

Note that the `lnx_percona_mysql` role has been added as it is a hard dependency for running this.
Then run `ansible-galaxy install -r requirements.yml`

### Playbook Additional Task

An addirional task can also be added in a playbook to install the role locally before including the role in the playbook itself

    ---
    - name: Sample Playbook
      hosts: all
      gather_facts: true
      # ...

      collections:
        # All the collections you'll need

      pre_tasks:
        - name: Install requirements
          delegate_to: localhost
          ansible.builtin.command: ansible-galaxy install -r {{ role_path }}/requirements.yml # <-- This is your requirements.yml file
        # Other tasks that you run BEFORE the roles

      roles:
        - { role: "lnx_percona_mysql" }
        # your other roles

      post_tasks:
        # other tasks that you run AFTER the roles

### Manual Installation

## Dependencies

This role depends on the `lnx_percona_repos` role to be able to run successfully.
